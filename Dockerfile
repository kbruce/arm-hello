FROM phusion/baseimage:jammy-1.0.0 AS builder

MAINTAINER Kane Bruce <kane.bruce@cern.ch>

# Use baseimage-docker's init system
CMD ["/sbin/my_init"]

# Base image and dependencies
RUN apt-get update && apt-get -y --no-install-recommends install \
	ccache \
	clang \
	clang-format \
	clang-tidy \
	cppcheck \
	curl \
	doxygen \
	gcc \
	git \
	graphviz \
	make \
	ninja-build \
	python3 \
	python3-pip \
	tar \
	unzip \
	vim \
	wget \
	tar \
	build-essential \
	libssl-dev

# Pull and build specific ver. cmake
RUN wget https://github.com/Kitware/CMake/releases/download/v3.15.0/cmake-3.15.0.tar.gz && \
	tar -zxvf cmake-3.15.0.tar.gz && \
	cd cmake-3.15.0 && \
	./bootstrap && \
	make && \
	make install 

# Install conan
RUN pip3 install conan

# Catch2 unit testing framework
RUN git clone https://github.com/catchorg/Catch2.git && \
	cd Catch2 && \
	cmake -Bbuild -H. -DBUILD_TESTING=OFF && \
	cmake --build build/ --target install

# GTest unit testing framework
# Disabled pthread support for GTest due to linking errors
RUN git clone https://github.com/google/googletest.git && \
	cd googletest && \
	cmake -Bbuild -Dgtest_disable_pthreads=1 && \
	cmake --build build --config Release && \
	cmake --build build --target install --config Release

# Vcpkg dependencies
RUN git clone https://github.com/microsoft/vcpkg -b 2020.06 && \
	cd vcpkg && \
	./bootstrap-vcpkg.sh -disableMetrics -useSystemBinaries

# Clean up APT when done
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Build the base binary!
RUN mkdir /app && cd /app && \
	cmake /builds/kbruce/arm-hello -DCMAKE_INSTALL_PREFIX=/runtime && \
	cmake --build . --target install

# Test the app...
RUN cd /app && ctest -j 4 -C Release -VV

###################################
# Core runner, install into image

FROM alpine:3.16 AS runtime
COPY --from=builder /runtime/ /

ENTRYPOINT ["ArmHello"]
