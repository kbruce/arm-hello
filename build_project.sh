#/bin/bash
cd "$(dirname "$0")"

unset -v _build_tgt
unset -v _install_loc

while getopts t:i: opt; do
	case $opt in
		t) _build_tgt=$OPTARG ;;
		i) _install_loc=$OPTARG ;;
		*)
			echo 'Error in command line parsing' >&2
			exit 1
	esac
done

shift "$(( OPTIND - 1 ))"

if [ -z "$_build_tgt" ]; then
	_build_tgt=""
fi
# default install location is the executing user's home dir
if [ -z "$_install_loc" ]; then
	_install_loc=$HOME
fi
# handle cleaning if specified
if [ "$_build_tgt" == 'clean' ]; then
	rm -rf build/
	mkdir build
fi

cd build
cmake .. -DArmHello_ENABLE_DOXYGEN=1 -DCMAKE_INSTALL_PREFIX=$_install_loc
cmake --build . --target install
