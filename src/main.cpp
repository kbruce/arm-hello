#include <iostream>

#include <armhello/tmp.hpp>

int main(int argc, char *argv[])
{
	std::cout << "[*] Hello world, from ARM | 1+2=" << ArmHello::add(1,2) << std::endl;
	return 0;
}
